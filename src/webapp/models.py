import random, string
from django.contrib.auth.models import User
from django.db import models
# Create your models here.


class Url(models.Model):
    url = models.CharField(max_length=2048)
    short = models.CharField(unique=True, max_length=64)
    user = models.ForeignKey(User, null=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        if len(self.url) > 32:
            return '{}...'.format(self.url[:32])
        else:
            return self.url

    def add(self, url):
        self.url = url
        self.short = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(8))
        self.save()


class Config(models.Model):
    option = models.CharField(max_length=32)
    value = models.CharField(max_length=1024)

    def __str__(self):
        return self.option

    def get_config():
        ret_entries = {}
        # for e in Config.objects.all():
        #     ret_entries[e.option] = e.value
        ret_entries['installation_name'] = 'Shorty'
        ret_entries['base_url'] = 'https://s.gnom.be/'
        return ret_entries
