from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import messages
from .models import Url, Config

# Create your views here.

conf = Config.get_config()


def start(request):
    return render(request, 'webapp/start.html', {'conf': conf, })


def new(request):
    if request.method == 'POST':
        short = Url()
        url = request.POST['url'].strip()
        if 'url' in request.POST:
            if len(url) <= 5:
                messages.warning(request, 'I need an url to shorten.')
                return start(request)
        else:
            messages.warning(request, 'I can\'t shorten nothing.')
            return start(request)
        short.add(url)
        return render(request, 'webapp/start.html', {'shorty': short, 'conf': conf, })
    else:
        return start(request)


def get_redirect(request, short):
    url = get_object_or_404(Url, short=short)
    return HttpResponseRedirect(url.url)