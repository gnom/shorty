from django.contrib import admin
from .models import Url, Config

# Register your models here.
admin.site.register(Url)
admin.site.register(Config)