FROM python:latest

EXPOSE 3456
COPY src /shorty
COPY requirements.txt /shorty
COPY deployment/shorty.sh /usr/local/bin/shorty
RUN chmod +x /usr/local/bin/shorty
RUN cd /shorty && pip install -r requirements.txt

VOLUME ["/data"]
